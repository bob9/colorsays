//
//  main.m
//  Color Says
//
//  Created by Melissa Pringle on 15/05/2014.
//  Copyright (c) 2014 Glenn Pringle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LSAppDelegate class]));
    }
}
