//
//  LSMyScene.h
//  Color Says
//

//  Copyright (c) 2014 Glenn Pringle. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface LSMyScene : SKScene

@property(assign, atomic) CFTimeInterval lastTime;

@property(nonatomic, retain) NSTimer* colorTimer;

@property(nonatomic, retain) NSArray* colorArray;
@property(nonatomic, retain) NSArray* theUIColorArray;


@property(nonatomic, retain) SKLabelNode *myLabel;
@property(nonatomic, retain) SKSpriteNode *spriteBackground;




@property(nonatomic, retain) UIColor* textColor; //TextColor
@property(nonatomic, retain) UIColor* textColorBackground; //BackgroundColor


@end
