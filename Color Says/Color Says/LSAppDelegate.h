//
//  LSAppDelegate.h
//  Color Says
//
//  Created by Melissa Pringle on 15/05/2014.
//  Copyright (c) 2014 Glenn Pringle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
