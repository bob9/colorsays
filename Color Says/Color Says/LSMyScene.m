//
//  LSMyScene.m
//  Color Says
//
//  Created by Melissa Pringle on 15/05/2014.
//  Copyright (c) 2014 Glenn Pringle. All rights reserved.
//

#import "LSMyScene.h"

@implementation LSMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.myLabel = [SKLabelNode labelNodeWithFontNamed:@"Times"];
        
        self.myLabel.text = @"GREEN";
        self.myLabel.fontSize = 50;
        self.myLabel.fontColor = [UIColor redColor];
        self.myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
            
                                            (self.frame.size.height/3)*2);
        
        

        
        
        [self addChild:self.myLabel];
        
        self.spriteBackground = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(250, 50)];
        self.spriteBackground.position = CGPointMake(self.myLabel.position.x, self.myLabel.position.y+15);
        [self addChild:self.spriteBackground];
        self.spriteBackground.zPosition = self.spriteBackground.zPosition-1;
        
        [self addColorButtons];
        

        

        self.colorTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(changeColor)
                                       userInfo:nil
                                        repeats:YES];
        
        self.colorArray = @[@"RED", @"GREEN", @"PURPLE", @"ORANGE", @"BLUE"];
        self.theUIColorArray = @[[UIColor redColor], [UIColor greenColor], [UIColor purpleColor], [UIColor orangeColor], [UIColor blueColor]];
    }
    return self;
}

-(int)generateRandomNumberBetweenMin:(int)min Max:(int)max
{
	return ( (arc4random() % (max-min+1)) + min );
}

-(void) changeColor
{
    [self.spriteBackground removeFromParent];
    self.textColor = [self.theUIColorArray objectAtIndex:[self generateRandomNumberBetweenMin:0 Max:4]];
    
    NSString* color = [self.colorArray objectAtIndex:[self generateRandomNumberBetweenMin:0 Max:4]];
    self.myLabel.text = color;
    self.myLabel.fontColor = self.textColor;
    
    self.textColorBackground = [self.theUIColorArray objectAtIndex:[self generateRandomNumberBetweenMin:0 Max:4]];
    self.spriteBackground = [SKSpriteNode spriteNodeWithColor:self.textColorBackground size:CGSizeMake(250, 50)];
    self.spriteBackground.position = CGPointMake(self.myLabel.position.x, self.myLabel.position.y+15);
    
    [self addChild:self.spriteBackground];
    self.spriteBackground.zPosition = self.spriteBackground.zPosition-1;
    
//    self.spriteBackground =
}


-(void)addColorButtons
{
    float x = self.frame.size.width/6;
    float y = (self.frame.size.height/3)*1;
    SKSpriteNode* block = [SKSpriteNode spriteNodeWithColor:[UIColor orangeColor] size:CGSizeMake(40, 40)];
    block.position = CGPointMake(x, y);
    block.name = @"orange";
    [self addChild:block];
    
    
    block = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(40, 40)];
    block.position = CGPointMake(x*2, y);
    block.name = @"purple";
    [self addChild:block];
    
    
    block = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(40, 40)];
    block.position = CGPointMake(x*3, y);
    block.name = @"red";
    [self addChild:block];
    
    
    block = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(40, 40)];
    block.position = CGPointMake(x*4, y);
    block.name = @"green";
    [self addChild:block];
    
    
    block = [SKSpriteNode spriteNodeWithColor:[UIColor blueColor] size:CGSizeMake(40, 40)];
    block.position = CGPointMake(x*5, y);
    block.name = @"blue";
    [self addChild:block];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    UITouch *touch = [touches anyObject];
    NSArray *nodes = [self nodesAtPoint:[touch locationInNode:self]];
    for (SKNode *node in nodes) {
        
        [self testColor:@"orange" withNode:node withColor:[UIColor orangeColor]];
        [self testColor:@"blue" withNode:node withColor:[UIColor blueColor]];
        [self testColor:@"red" withNode:node withColor:[UIColor redColor]];
        [self testColor:@"green" withNode:node withColor:[UIColor greenColor]];
        [self testColor:@"purple" withNode:node withColor:[UIColor purpleColor]];

    }
}

-(void) testColor:(NSString*)colorString withNode:(SKNode*)node withColor:(UIColor*)color
{
    if ([node.name isEqualToString:colorString])
    {
        if([self.textColor isEqual:color])
        {
            NSLog(@"%@", colorString);
        }
        else
        {
            NSLog(@"%@ fail", colorString);
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    self.lastTime = currentTime;
    
}

@end
